ARG ARCH=
FROM ${ARCH}golang:1.16-alpine as builder
WORKDIR /vaultbot
COPY / /vaultbot/
RUN go get .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o vaultbot .

FROM ${ARCH}alpine:latest
WORKDIR /root/
COPY --from=builder /vaultbot/vaultbot .
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
ENTRYPOINT ["./vaultbot"]
