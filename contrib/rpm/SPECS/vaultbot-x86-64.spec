%define branchname %{?_branchname}%{?!_branchname:0}
%define releaseversion %{?_releaseversion}%{?!_releaseversion:0}

Name: vaultbot
Version: %{branchname}
Release: %{releaseversion}
License: MIT
URL: https://gitlab.com/msvechla/vaultbot
Requires: glibc
BuildArch: x86_64
ExclusiveOS: linux
Group: system
Summary: Lightweight Hashicorp Vault PKI client, built for infrastructure automation

Source0: vaultbot-linux-amd64
Source1: vaultbot@.service
Source2: vaultbot@.timer
Source3: vaultbot.conf
Source4: README.md
Source5: LICENSE.md

%description
Lightweight Hashicorp Vault PKI client, built for infrastructure automation.
Automatically request and renew certificates generated inside vault via the PKI backend.

%prep
# nothing
exit 0

%install
# clean
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_defaultdocdir}/vaultbot
mkdir -p %{buildroot}%{_sysconfdir}/vaultbot

cp %{SOURCE0} %{buildroot}%{_bindir}/vaultbot
cp %{SOURCE1} %{buildroot}%{_unitdir}/vaultbot@.service
cp %{SOURCE2} %{buildroot}%{_unitdir}/vaultbot@.timer
cp %{SOURCE3} %{buildroot}%{_sysconfdir}/vaultbot/vaultbot.conf
cp %{SOURCE4} %{buildroot}%{_defaultdocdir}/vaultbot/README.md
cp %{SOURCE5} %{buildroot}%{_defaultdocdir}/vaultbot/LICENSE.md

%clean
rm -rf %{buildroot}

%files
%defattr(644, root, root, -)
%attr(755, -, -) %{_bindir}/vaultbot
%{_unitdir}/vaultbot@.service
%{_unitdir}/vaultbot@.timer
%config %{_sysconfdir}/vaultbot/vaultbot.conf
%docdir %{_sysconfdir}/vaultbot/
%doc %{_defaultdocdir}/vaultbot/README.md
%license %{_defaultdocdir}/vaultbot/LICENSE.md

%changelog
